#############################################################################
#
# Airbus Flight Management.
#
# Abstract:
# 
# 
# Author: S.Hamilton Jan 2010
#
#
#   Copyright (C) 2010 Scott Hamilton
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

trace = 1;







var AirbusFMS = {
   
   new : func(guidance) {
     m = {parents : [AirbusFMS]};
     m.afs = guidance;
     m.FMSnode = props.globals.getNode("/instrumentation/fms",1);
     m.activeFpln = m.FMSnode.getNode("active-fpln",1);
     m.depDB = nil;
     m.arvDB = nil;

     setlistener("/sim/signals/fdm-initialized", func m.init());
     print("Airbus FMS initalised");
     return m;
   },

#############################################################################
#  output a debug message to stdout or file.
#############################################################################
tracer : func(msg) {
  var timeStr = getprop("/sim/time/gmt-string");
  var curAltStr = getprop("/position/altitude-ft");
  var curVnav    = me.afs.vnav.getValue();
  var curLnav    = me.afs.lnav.getValue();
  var curSpd     = me.afs.spd.getValue();
  var athrStr   = getprop("/instrumentation/flightdirector/at-on");
  var ap1Str     = getprop("/instrumentation/flightdirector/ap");
  var altHold = getprop("/autopilot/settings/target-altitude-ft");
  var vsHold  = getprop("/autopilot/settings/vertical-speed-fpm");
  var spdHold = getprop("/autopilot/settings/target-speed-kt");
  if (curVnav == nil) curVnav = "0";
  if (curLnav == nil) curLnav = "0";
  if (curSpd  == nil) curSpd  = "0";
  if (trace > 0) {
    print("[FMS] time: "~timeStr~" alt: "~curAltStr~", - "~msg);
    if (trace > 1) {
      print("[FMS] vnav: "~me.afs.vertical_text[curVnav]~", lnav: "~me.afs.lateral_text[curLnav]~", spd: "~me.afs.spd_text[curSpd]);
    }
  }
},


   init : func() {
      me.tracer("FDM ready");
      settimer(func me.update(), 0);
      settimer(func me.slow_update(), 0);
    },

   ### for high prio tasks ###
   update : func() {
     settimer(func me.update(), 1);
   }, 

   ### for low prio tasks ###
   slow_update : func() {
     me.tracer("FMS update");
     settimer(func me.slow_update(), 3);
   },

};



#################################################
#   basic mathematic functions not in Nasal :(
#################################################
#var pow = func(x, y) { 
#  math.exp(y * math.ln(x))
#}

var acos = func(x) { 
  math.atan2(math.sqrt(1-x*x), x) 
}

var asin = func(x) {
 math.atan2(x,math.sqrt(math.pow(2,1-x)));
}

var degMin2rad = func(deg, min) {
  var d1 = deg+(min/60);
  return d1*(math.pi/180);
}

var nm2rad = func(d) {
 return d*math.pi/(180*60);
}


################################################################
#  calculate great circle distance using degrees
################################################################
var calcGCDistanceDeg = func(plat1, plon1, plat2, plon2) {
 var lat1 = plat1*DEG2RAD;
 var lon1 = plon1*DEG2RAD;
 var lat2 = plat2*DEG2RAD;
 var lon2 = plon2*DEG2RAD;
 #print("lat1: "~lat1~", lon1: "~lon1~", lat2: "~lat2~", lon2: "~lon2);
 return calcGCDistance(lat1, lon1, lat2, lon2);
}

####################################################################
# calc Great Circle distance using radians, returns NM
##################################################################
var calcGCDistance =func(lat1, lon1, lat2, lon2) {
 d1 = math.sin((lat1-lat2)/2);
 #print("d1: "~d1);
 p1 = math.pow(2, d1);
 #print("p1: "~p1);
 dd2 = (math.sin((lon1-lon2)/2));
 d2 = math.cos(lat1)*math.cos(lat2)*dd2;
 #print("d2: "~d2);
 p2 = math.pow(2, d2);
 #print("p2: "~p2);
 d3 = math.sqrt(p1 + p2);
 #print("d3: "~d3);
 as4 = asin(d3);
 #print("as4: "~as4);
 das4rad = 2*as4;
 das4nm = das4rad*180*60/math.pi;
 ### d=2*asin(sqrt((sin((lat1-lat2)/2))^2 + cos(lat1)*cos(lat2)*(sin((lon1-lon2)/2))^2))
 return das4nm;
}

####################################################################
# calculate great circle true course using radians and distance in nm.
####################################################################
var calcGCTrueCourse = func(d, lat1, lon1, lat2, lon2) {
  var tc1 = 0;
  if(math.sin(lon2-lon1)<0) {      
   tc1=acos((math.sin(lat2)-math.sin(lat1)*math.cos(d))/(math.sin(d)*math.cos(lat1)));
  } else {   
   var d1 = (math.sin(lat2)-math.sin(lat1)*math.cos(d));
   var d2 = (math.sin(d)*math.cos(lat1));
   var dd1 = d1/d2;
   tracer("[calcTC] d1: "~d1~", d2: "~d2~", dd1: "~dd1);
   tc1=2*math.pi-math.acos(dd1);
  }
  return tc1;
}

#######################################################################
#  calculate lat,lon of point that is d distance from lat,lon by tc true course in radians
#######################################################################
var calcDistancePoint = func(tc, d, lat1, lon1) {
  print("tc: "~tc~", d: "~d~", lat1: "~lat1~", lon1: "~lon1);
  var lat = math.asin(math.sin(lat1)*math.cos(d)+math.cos(lat1)*math.sin(d)*math.cos(tc));
  #print("lat: "~lat);
  var dlon = math.atan2(math.sin(tc)*math.sin(d)*math.cos(lat1),math.cos(d)-math.sin(lat1)*math.sin(lat));
  #print("dlon: "~dlon);
  var lon = math.mod(lon1-dlon+math.pi,2*math.pi )-math.pi;
  #print("lon: "~lon);
  var latDeg = lat*RAD2DEG;
  var lonDeg = lon*RAD2DEG;
  tracer("[calcDistPoint] latDeg: "~latDeg~", lonDeg: "~lonDeg);
  var dest = geo.Coord.new();
  dest.set_latlon(latDeg, lonDeg, 0);
  #print("final lat: "~latDeg~", lon: "~lonDeg);
  return dest;
}

var calcDistancePointDeg = func(tc, d, lat1, lon1) {
  return calcDistancePoint(tc*DEG2RAD, nm2rad(d), (lat1*DEG2RAD), (lon1*DEG2RAD));
}

######################################################################
# calc straight line course between two points
######################################################################
var calcOrthHeadingDeg = func(lat1, lon1, lat2, lon2) {
 tracer("[calcOrth] lat1: "~lat1~", lon1: "~lon1~", lat2: "~lat2~", lon2: "~lon2);
 lat1 *= DEG2RAD;
 lon1 *= DEG2RAD;
 lat2 *= DEG2RAD;
 lon2 *= DEG2RAD;
 
 sin_lat1 = math.sin(lat1);
 cos_lat1 = math.cos(lat1);
 sin_lat2 = math.sin(lat2);
 cos_lat2 = math.cos(lat2);
 dlon = lon2-lon1;
   
 Aorth = math.atan2(math.sin(dlon)*cos_lat2, cos_lat1*sin_lat2-sin_lat1*cos_lat2*math.cos(dlon));
 while ( Aorth >= TWOPI ) {Aorth -= TWOPI};
 if(Aorth<0) Aorth+= TWOPI;
 return (Aorth*RAD2DEG);
}

