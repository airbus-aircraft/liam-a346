#############################################################################
# Abstract:
# A340 Flight Director/Autopilot controller.
#
# Author:
# Liam Gathercole (based on Syd Adams 777)
#
# Modification History
# Date		Who		Version		What
# Jan 2010	S.Hamilton			Airbus managed and selected modes
#
#
# speed modes  = THR,THR REF, IDLE,HOLD,SPD;
# roll modes = HDG SEL,HDG HOLD, LNAV,LOC,ROLLOUT,TO/GA,TRK SEL, TRK HOLD,ATT;
# pitch modes TO/GA,ALT,V/S,VNAV PTH,VNAV SPD,VNAV ALT,G/S,FLARE,FLCH SPD,FPA; 
# FPA range: -9.9 ~ 9.9 degrees
# VS range : -8000 ~ 6000
# ALT range  : 0 ~ 50,000
#
#  # A380 modes (s-selected, m-managed)
# lnav 0=off, 1=HDG(s), 2=TRACK(s), 3=NAV1/LOC(m), 4=FMS(m), 5=RWY(m)
# vnav 0=off, 1=ALT(s), 2=V/S(s), 3=OP CLB(s), 4=FPA(s), 5=OP DES(s), 6=CLB(m), 7=ALT(m), 8=DES=(m), 9=G/S(m), 10=SRS(m),11=LEVEL
# athr 0=idle, 1=CL, 2=FLX, 3=TOGA
# spd  0=off, 1=TOGA, 2=FLEX, 3=THR CLB, 4=SPEED(s), 5=MACH(s), 6=CRZ(m), 7=THR IDL(m - DES)

# 
#############################################################################

#Usage : var FD = AirbusGuidance.new(flightdirector property);
# var FD = AirbusGuidance.new("instrumentation/FD");

    MODE_SELECTED = "S";
    MODE_MANAGED  = "M";

    VNAV_OFF    = 0;
    VNAV_ALT    = 1;
    VNAV_VS     = 2;
    VNAV_FPA    = 3;
    VNAV_OPCLB  = 4;
    VNAV_OPDES  = 5;
    VNAV_SRS    = 6;
    VNAV_CLB    = 7;
    VNAV_ALTCRZ = 8;
    VNAV_DES    = 9;
    VNAV_GS     = 10;

    LNAV_OFF    = 0;
    LNAV_HDG    = 1;
    LNAV_TRACK  = 2;
    LNAV_NAV    = 3;
    LNAV_LOC    = 4;
    LNAV_RWY    = 5;

    SPD_OFF     = 0;
    SPD_SPEED   = 1;
    SPD_MACH    = 2;
    SPD_THRIDLE = 3;
    SPD_MANTOGA = 4;
    SPD_MANFLX  = 5;
    SPD_MANDTO  = 6;
    SPD_MANMCT  = 7;
    SPD_MANTHR  = 8;
    SPD_THRMCT  = 9;
    SPD_THRCLB  = 10;
    SPD_THRDES  = 11;
    SPD_THRLVR  = 12;

    trace=1;

var AirbusGuidance = {

    new : func(prop) {
        var m = {parents:[AirbusGuidance]};

	m.lateral_text = ["","HDG","TRACK","NAV","LOC","RWY"];
	m.vertical_text = ["","ALT","V/S","FPA","OP CLB","OP DES","SRS","CLB","ALT CRZ","DES","G/S"];
	m.spd_text = ["","SPEED","MACH","THR IDLE","MAN TOGA","MAN FLX","MAN DTO","MAN MCT","MAN THR","THR MCT","THR CLB","THR DES","THR LVR"];
        
        m.FDnode = props.globals.getNode(prop,1);
        m.DH=m.FDnode.getNode("DH",1);
        m.DH.setDoubleValue(400);
        m.Bank_limit_max=m.FDnode.getNode("bank-limit-max",1);
        m.Bank_limit_max.setDoubleValue(30);
        m.Bank_limit_min=m.FDnode.getNode("bank-limit-min",1);
        m.Bank_limit_min.setDoubleValue(-30);
        m.Bank_limit_switch=m.FDnode.getNode("bank-limit-switch",1);
        m.Bank_limit_switch.setIntValue(0);
	###  mode - 0 = selected / 1 = managed
	###  in managed mode display = 0.
	m.spd_display = m.FDnode.getNode("spd-display",1);
	m.spd_mode    = m.FDnode.getNode("spd-mode",1);
        m.speed_curr_idx = m.FDnode.getNode("current-spd-idx",1);
	m.hdg_display = m.FDnode.getNode("hdg-display",1);
	m.hdg_mode    = m.FDnode.getNode("hdg-mode",1);
        m.lateral_curr_idx = m.FDnode.getNode("current-lateral-idx",1);
	m.alt_display = m.FDnode.getNode("alt-display",1);
	m.alt_mode    = m.FDnode.getNode("alt-mode",1);
        m.vertical_curr_idx = m.FDnode.getNode("current-vertical-idx",1);
	m.vs_display = m.FDnode.getNode("vs-display",1);
	m.vs_mode    = m.FDnode.getNode("vs-mode",1);
        m.set_spd_mode(MODE_MANAGED);
        m.set_hdg_mode(MODE_SELECTED);
        m.set_alt_mode(MODE_MANAGED);
        m.set_vs_mode(MODE_MANAGED);

        m.speed_curr_idx.setValue(0);
        m.lateral_curr_idx.setValue(0);
        m.vertical_curr_idx.setValue(0);
        m.flight_phase = 0;

        m.FPA=m.FDnode.getNode("FP-angle",1);
        m.FPA.setDoubleValue(9.9);
        m.MACHdisplay=m.FDnode.getNode("mach-display",1);
        m.MACHdisplay.setBoolValue(0);
        m.TRKdisplay=m.FDnode.getNode("track-display",1);
        m.TRKdisplay.setBoolValue(0);
        m.FPAdisplay=m.FDnode.getNode("fpa-display",1);
        m.FPAdisplay.setBoolValue(0);
        m.lnav=m.FDnode.getNode("lnav",1);
        m.lnav.setIntValue(0);
        m.vnav=m.FDnode.getNode("vnav",1);
        m.vnav.setIntValue(0);
        m.VS=m.FDnode.getNode("vs-fpm",1);
        m.VS.setDoubleValue(0);
        m.spd=m.FDnode.getNode("speed",1);
        m.spd.setIntValue(0);
        m.Dist=m.FDnode.getNode("signal-distance",1);
        m.Dist.setDoubleValue(0);
        m.serviceable=m.FDnode.getNode("serviceable",1);
        m.serviceable.setBoolValue(0);
        m.FP_alt=m.FDnode.getNode("FP-alt",1);
        m.FP_alt.setDoubleValue(10000);
        m.AP_disengage=m.FDnode.getNode("AP-disengage",1);
        m.AP_disengage.setBoolValue(0);
        m.AP_mode=m.FDnode.getNode("AP-mode",1);
        m.AP_mode.setBoolValue(0);
        m.AP_annun=m.FDnode.getNode("AP-annun",1);
        m.AP_annun.setValue("   ");
	m.AP_hdg = props.globals.getNode("autopilot/locks/heading",1);
        m.AP_hdg.setValue(m.lateral_text[m.lnav.getValue()]);
        m.AP_hdg_bug = props.globals.getNode("autopilot/settings/heading-bug-deg",1);
        m.AP_hdg_bug.setValue(360);
        m.AP_alt = props.globals.getNode("autopilot/locks/altitude",1);
        m.AP_alt.setValue(m.vertical_text[m.vnav.getValue()]);
        m.AP_alt_setting = props.globals.getNode("autopilot/settings/target-altitude-ft",1);
        m.AP_alt_setting.setDoubleValue(10000);
        m.AP_vs_setting = props.globals.getNode("autopilot/settings/vertical-speed-fpm",1);
        m.AP_vs_setting.setDoubleValue(0);
        m.AP_pitch_setting = props.globals.getNode("autopilot/settings/target-pitch-deg",1);
        m.AP_pitch_setting.setDoubleValue(0);
        m.AP_spd = props.globals.getNode("autopilot/locks/speed",1);
        m.AP_spd.setValue(m.spd_text[m.spd.getValue()]);
        m.AP_spd_setting = props.globals.getNode("autopilot/settings/target-speed-kt",1);
        m.AP_spd_setting.setDoubleValue(200);
        m.AP_mach_setting = props.globals.getNode("autopilot/settings/target-speed-mach",1);
        m.AP_mach_setting.setDoubleValue(0.4);
        m.AP_toggle = props.globals.getNode("autopilot/locks/passive-mode",1);
        m.AP_toggle.setBoolValue(1);

        m.AP_pitch_active = props.globals.getNode("autopilot/locks/pitch-engaged",1);
        m.AP_pitch_active.setBoolValue(1);
        m.AP_roll_active = props.globals.getNode("autopilot/locks/roll-engaged",1);
        m.AP_roll_active.setBoolValue(1);
        m.AP_speed_active = props.globals.getNode("autopilot/locks/speed-engaged",1);
        m.AP_speed_active.setBoolValue(1);


        m.has_gs = props.globals.getNode("instrumentation/nav/has-gs",1);
        m.is_localizer = props.globals.getNode("instrumentation/nav/nav-loc",1);
        setlistener("/sim/signals/fdm-initialized", func m.init());
        print("Airbus Guidance initalised");
        return m;
    },

#############################################################################
#  output a debug message to stdout or file.
#############################################################################
tracer : func(msg) {
  var timeStr = getprop("/sim/time/gmt-string");
  var curAltStr = getprop("/position/altitude-ft");
  #var curVnav   = getprop("/instrumentation/flightdirector/vnav");
  #var curLnav   = getprop("/instrumentation/flightdirector/lnav");
  #var curSpd    = getprop("/instrumentation/flightdirector/spd");
  var curVnav    = me.vnav.getValue();
  var curLnav    = me.lnav.getValue();
  var curSpd     = me.spd.getValue();
  var athrStr   = getprop("/instrumentation/flightdirector/at-on");
  var ap1Str     = getprop("/instrumentation/flightdirector/ap");
  var altHold = getprop("/autopilot/settings/target-altitude-ft");
  var vsHold  = getprop("/autopilot/settings/vertical-speed-fpm");
  var spdHold = getprop("/autopilot/settings/target-speed-kt");
  if (curVnav == nil) curVnav = "0";
  if (curLnav == nil) curLnav = "0";
  if (curSpd  == nil) curSpd  = "0";
  if (trace > 0) {
    print("[AirbusFG] time: "~timeStr~" alt: "~curAltStr~", - "~msg);
    if (trace > 1) {
      print("[AirbusFG] vnav: "~me.afs.vertical_text[curVnav]~", lnav: "~me.afs.lateral_text[curLnav]~", spd: "~me.afs.spd_text[curSpd]);
    }
  }
},

    init : func() {
      me.tracer("FDM ready");
      settimer(func me.update(), 0);
      settimer(func me.slow_update(), 0);
    },

#### managed and selected modes ####
    set_spd_mode : func(mode) {
      var display = 1;
      if (mode == "M") {
        display = 0;
      }
      me.spd_display.setBoolValue(display);
      me.spd_mode.setValue(mode);
    },

    set_hdg_mode : func(mode) {
	var display = 1;
        if (mode == "M") {
          display = 0;
        }
        me.hdg_display.setBoolValue(display);
        me.hdg_mode.setValue(mode);
    },

    set_alt_mode : func(mode) {
	var display = 1;
        if (mode == "M") {
          display = 0;
        }
        me.alt_display.setBoolValue(display);
        me.alt_mode.setValue(mode);
    },

    set_vs_mode : func(mode) {
	var display = 1;
        if (mode == "M") {
          display = 0;
        }
        me.vs_display.setBoolValue(display);
        me.vs_mode.setValue(mode);
    },

#### check FD button presses ####
    mode_set: func(str){
        var mode2 = str;
        var fdmode = me.serviceable.getValue();
            
        if(mode2 == "FD"){
            fdmode = 1-fdmode;
            
            if(fdmode==1){
                me.lnav.setValue(6);
                me.vnav.setValue(1);
                me.spd.setValue(5);
                me.AP_spd_setting.setValue(200);
                me.AP_vs_setting.setValue(1500);
                me.AP_pitch_setting.setValue(10);
		me.AP_annun.setValue("   CMD");
            }else{
            me.lnav.setValue(1);
            me.vnav.setValue(9);
            me.spd.setValue(0);
	    me.AP_annun.setValue("      ");
            }
            me.AP_hdg.setValue(me.lnav_text[me.Lnav.getValue()]);
            me.AP_alt.setValue(me.vnav_text[me.Vnav.getValue()]);
            me.AP_spd.setValue(me.spd_text[me.Spd.getValue()]);
            me.serviceable.setValue(fdmode);
            me.AP_mode.setValue(fdmode);
            return;
        }

        if(mode2 == "AP-ON"){
            tmpalt=getprop("position/gear-agl-ft");
            var apmode = 1- me.AP_toggle.getValue();
            if(apmode ==0){
                if(fdmode==0){
                    me.lnav.setValue(0);
                    me.AP_hdg.setValue(me.lnav_text[0]);
                    me.vnav.setValue(0);
                    me.AP_alt.setValue(me.vnav_text[0]);
		    me.AP_annun.setValue("AP ENG");
                }
            if(tmpalt<400)apmode =1;
            if(me.AP_disengage.getValue())apmode=1;
	    me.AP_annun.setValue("   CMD");
            }
            me.AP_toggle.setValue(apmode);
            return;
        }

        if(mode2 == "APP"){
            me.lnav.setValue(4);
            me.AP_hdg.setValue(me.lnav_text[4]);
            me.vnav.setValue(7);
            me.AP_alt.setValue(me.vnav_text[7]);
            return;
        }


        if(mode2 == "AP-DISENGAGE"){
            var ap_dis =me.AP_disengage.getValue();
            ap_dis=1-ap_dis;
            me.AP_disengage.setValue(ap_dis);
            if(ap_dis ==1)me.AP_toggle.setValue(1);
            return;
        }

        if(mode2 == "HDG"){
            var lm=1;
            if(me.Lnav.getValue()==lm)lm=0;
            me.lnav.setValue(lm);
            me.AP_hdg.setValue(me.lnav_text[lm]);
            return;
        }

        if(mode2 == "LNAV"){
            var lm=3;
            var RM = getprop("autopilot/route-manager/route/num");
            if(me.lnav.getValue()==lm or RM ==0)lm=1;
            me.lnav.setValue(lm);
            me.AP_hdg.setValue(me.lnav_text[lm]);
            return;
        }

        if(mode2 == "LOC"){
            var lm=4;
            me.lnav.setValue(lm);
            me.AP_hdg.setValue(me.lnav_text[lm]);
            return;
        }

        if(mode2 == "FLCH"){
            var vn=8;
            me.vnav.setValue(vn);
            me.AP_alt.setValue(me.vnav_text[vn]);
            return;
        }

        if(mode2 == "VNAV"){
            var vn=6;
            if(getprop("autopilot/route-manager/route/num") ==0){
                vn=1;
            }else{
            var tmpalt =getprop("autopilot/route-manager/route/wp/altitude-ft");
            if(tmpalt < 0)tmpalt=35000;
            me.FP_alt.setValue(tmpalt);
            me.AP_spd_setting.setValue(250);
            }
            me.vnav.setValue(vn);
            me.AP_alt.setValue(me.vnav_text[vn]);
        return;
        }

        if(mode2 == "ALT"){
            var vn=2;
            var tmpalt2 =getprop("position/altitude-ft");
            tmpalt2 = int(tmpalt2 *0.01) * 100;
            me.AP_alt_setting.setValue(tmpalt2);
            me.vnav.setValue(vn);
            me.AP_alt.setValue(me.vnav_text[vn]);
            return;
        }

        if(mode2 == "VS"){
            var vn=3;
            me.AP_vs_setting.setValue(getprop("velocities/vertical-speed-fps") * 60);
            me.vnav.setValue(vn);
            me.AP_alt.setValue(me.vnav_text[vn]);
            return;
        }

        if(mode2 == "AT"){
            var sp=5;
            var spon=me.AP_speed_active.getValue();
            sp =sp * spon;
            me.spd.setValue(sp);
            me.AP_spd.setValue(me.spd_text[sp]);
            return;
        }

    },


    lnav_check: func(lm){
    var md = me.Fms.getBoolValue();
    if(md){
        if(lm==2)lm = 6;
    }elsif(lm==4){
            if(!me.is_localizer.getValue())lm=2;
        }
    if(me.lnav.getValue()==lm)lm=0;
    me.lnav.setValue(lm);
    me.AP_hdg.setValue(me.lnav_text[lm]);
    },

    vnav_check: func(vmode){
    if(me.vnav.getValue()!=vmode){
        me.vnav.setValue(vmode);
    }else{
        me.vnav.setValue(0);
        }
    },

    spd_check: func(smode){
    if(me.spd.getValue()!=smode){
        me.spd.setValue(smode);
    }else{
        me.spd.setValue(0);
        }
    },

    pitch_wheel : func(dir){
        var vm=me.AP_alt.getValue();
        if(vm=="FPA"){
            var temp_pitch = me.FPA.getValue();
            temp_pitch +=(dir *0.01);
            if(temp_pitch > 9.9)temp_pitch = 9.9;
            if(temp_pitch < -9.9)temp_pitch = -9.9;
            me.FPA.setValue(temp_pitch);
            }elsif(vm=="V/S"){
                var temp_vs = me.AP_vs_setting.getValue();
                   temp_vs +=(dir *10);
                   if(temp_vs < -8000) temp_vs= -8000;
                   if(temp_vs > 6000) temp_vs= 6000;
                me.AP_vs_setting.setValue(temp_vs);
                }
    return;
    },

    ias_tune : func(as){
        var ias = 0;
        if(me.MACHdisplay.getBoolValue()){
            ias=me.AP_mach_setting.getValue();
            ias += as *0.01;
            if(ias > 0.99) ias = 0.99;
            if(ias < 0.0) ias = 0.0;
            me.AP_mach_setting.setValue(ias);
        }else{
            ias=me.AP_spd_setting.getValue();
            ias += as;
            if(ias <0)ias=0;
            if(ias> 399)ias =399;
            me.AP_spd_setting.setValue(ias);
        }
        return;
    },

    hdg_tune : func(hdg){
        var hd=me.AP_hdg_bug.getValue();
        hd += hdg;
        if(hd <1)hd+=360;
        if(hd> 360)hd-=360;
        me.AP_hdg_bug.setValue(hd);
        return;
    },

    alt_tune : func(asel){
        var alt=me.AP_alt_setting.getValue();
        alt += asel;
        if(alt <0)alt=0;
        if(alt> 50000)alt=50000;
        me.AP_alt_setting.setValue(alt);
        return;
    },

    bank_limit : func(bnk){
        var bank=me.Bank_limit_switch.getValue();
        bank += bnk;
        if(bank >5)bank=5;
        if(bank <0)bank=0;
        me.Bank_limit_switch.setValue(bank);
        var tmp = bank * 5;
        if(tmp > 0){
            me.Bank_limit_min.setValue(-1 * tmp);
            me.Bank_limit_max.setValue( tmp);
        }else{
            me.Bank_limit_min.setValue(-30);
            me.Bank_limit_max.setValue( 30);
        }
    return;
    },

    AT_arm : func{
        var arm=me.AP_speed_active.getValue();
        arm=1-arm;
        me.AP_speed_active.setValue(arm);
    return;
    },

    toggle_display_mode : func(dm) {
        if(dm=="mach"){
            var mc = me.MACHdisplay.getValue();
            mc =1-mc;
            if(getprop("velocities/mach") <0.40001)mc =0;
            if(mc==1){
                if(me.spd.getValue() ==1){
                    me.spd.setValue(2);
                    me.AP_spd.setValue(me.spd_text[2]);
                }
            }
            if(mc==0){
                if(me.spd.getValue() ==2){
                    me.spd.setValue(1);
                    me.AP_spd.setValue(me.spd_text[1]);
                }
            }
            me.MACHdisplay.setValue(mc);
        }elsif(dm=="trk"){
            var trk = me.TRKdisplay.getValue();
            trk =1-trk;
            me.TRKdisplay.setValue(trk);
        }elsif(dm=="fpa"){
            var fpa = me.FPAdisplay.getValue();
            fpa =1-fpa;
            
            me.FPAdisplay.setValue(fpa);
        }
    return;
    },

    ### PRIM update loop ###
    update : func() {
      var falt = getprop("position/altitude-ft");
      var tmppch = 8.0 - (falt * 0.0001);
      me.FPA.setValue(tmppch);
      settimer(func me.update(), 0.5);
    },

    ### for low priority tasks ###
    slow_update : func() {
      me.tracer("SEC update");
      settimer(func me.slow_update(), 3);
    },

};

