Airbus A340-600, For Flightgear Simulator.
Author(s):
   Liam Gathercole
-------------------------------------------------------------------
Installation:

Once you have Downloaded the .zip file of this aircraft, Extract it onto your hard drive. Then Extract it, and place the containing "A340-600" File into your Flightgear/Data/Aircraft/ folder.

Load up Flightgear and select the Airbus A340-600.

-------------------------------------------------------------------
Flying the plane

Experience in flying in Flightgear with other long-range jetliners such as the 777-200, 787, etc is recommended, as there is yet no tutorials in this aircraft itself, but you can find basic help on starting up in the Help dialog (By pressing the "?" key in the simulator). To start the engines automatically, Click on the Autostart button under the A340-600 button.

Contrails:
Aircraft Vapour trails will appear automatically as you reach 19,000ft, where the temperature is cool enough for them to form.

Lights:
All safety lights are currently on all the time, unless your engines are shutdown. The lights will be on at startup, ready for you to depart safely.

Tyre Smoke:
Tyre smoke will occur as your tyres reach the runway surface. In the event of extreme landings, these may cause mild loss of vision on external views, so cockpit view on landing is reccomended.

Aircrew:
SFX hostess voices and seatbelt alarms can be activated under your A340-600 button in the simulator. These will then play should you be in an interior view, or quietly if you are in an exterior view.

Liveries:
You can switch between multiple airliner repaints by clicking on the A340-600 button in the simulator, then clicking on Select Livery as the dropdown box appears. This will open a small box providing a list of airlines to switch between. All airline repaints in this package are operated in real life, and it is recommended that you please do not create fictional liveries for this aircraft, to keep the simulator as realistic as possible.

-------------------------------------------------------------------
Have any problems with this aircraft? please share your bugs/suggestions/comments/etc on the Flightgear official Forums.

This model is protected under GNU GPL as stated in the GNU GPL text file.